<?php

namespace Drupal\extension_reference\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'extension_reference' formatter.
 *
 * @FieldFormatter(
 *   id = "extension_reference",
 *   label = @Translation("Extension Reference"),
 *   field_types = {
 *     "extension_reference"
 *   }
 * )
 */
class ExtensionReferenceFormatter extends OptionsDefaultFormatter {

}
