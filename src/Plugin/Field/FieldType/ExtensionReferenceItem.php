<?php

namespace Drupal\extension_reference\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Defines the 'extension_reference' field type.
 *
 * @FieldType(
 *   id = "extension_reference",
 *   label = @Translation("Extension"),
 *   category = @Translation("Reference"),
 *   default_widget = "extension_reference",
 *   default_formatter = "extension_reference"
 * )
 */
class ExtensionReferenceItem extends FieldItemBase implements OptionsProviderInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = ['target_type' => ''];
    return $settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $this->getSetting('target_type'),
      '#required' => TRUE,
      '#options' => [
        '' => $this->t('Select one'),
        'module' => $this->t('Module'),
        'theme' => $this->t('Theme'),
        'profile' => $this->t('Profile'),
      ],
      '#description' => $this->t('Select the type of extension to reference.')
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('target_id')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['target_id'] = DataDefinition::create('string')
      ->setLabel(t('Extension'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      'target_id' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'The referenced extension.',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
      'indexes' => [
        'value' => ['target_id'],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'target_id';
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    // @todo randomize the sample, taking performance in mind.
    return [
      'target_id' => 'bartik',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(AccountInterface $account = NULL) {
    return $this->getSettableValues($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL) {
    return $this->getSettableOptions($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableValues(AccountInterface $account = NULL) {
    return array_keys($this->getSettableOptions($account));
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL) {
    $target_type = $this->getSetting('target_type');
    $options = [];
    try {
      /** @var \Drupal\Core\Extension\ExtensionList $extension_list */
      $extension_list = \Drupal::service('extension.list.' . $target_type);
      foreach ($extension_list->getList() as $extension) {
        $options[$extension->getName()] = $extension->getName();
      }
    } catch (ServiceNotFoundException $e) {
      watchdog_exception('extension_reference', $e);
    }
    return $options;
  }

}
