<?php

namespace Drupal\extension_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'extension_reference' widget.
 *
 * @FieldWidget(
 *   id = "extension_reference",
 *   label = @Translation("Extension Reference"),
 *   field_types = {
 *     "extension_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class ExtensionReferenceWidget extends OptionsSelectWidget {

}
